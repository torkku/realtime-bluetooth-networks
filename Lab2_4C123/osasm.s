;/*****************************************************************************/
; OSasm.s: low-level OS commands, written in assembly                       */
; Runs on LM4F120/TM4C123/MSP432
; Lab 2 starter file
; February 10, 2016
;


        AREA |.text|, CODE, READONLY, ALIGN=2
        THUMB
        REQUIRE8
        PRESERVE8

        EXTERN  RunPt            ; currently running thread
        EXPORT  StartOS
        EXPORT  SysTick_Handler
        IMPORT  Scheduler

; With ASM Round-Robin Scheduler

;SysTick_Handler                ; 1) Saves R0-R3,R12,LR,PC,PSR
;    CPSID   I                  ; 2) Prevent interrupt during switch

;    PUSH    {R4-R11}           ; 3) Save remaining registers, R4-R11
;    LDR     R0, =RunPt         ; 4) Load pointer to RunPt (current thread) into R0
;    LDR     R1, [R0]           ;    R1 = RunPt
;    STR     SP, [R1]           ; 5) Save SP into TCB (RunPt->sp)
;    LDR     R1, [R1, #4]       ; 6) Load pointer to next TCB into R1 (RunPt->next)
;    STR     R1, [R0]           ;    Update RunPt (RunPt = RunPt->next)
;    LDR     SP, [R1]           ; 7) Load into SP the SP of the new thread (SP = RunPt->sp)
;    POP     {R4-R11}           ; 8) restore register R4-R11 from stack

;    CPSIE   I                  ; 9) tasks run with interrupts enabled
;    BX      LR                 ; 10) restore R0-R3,R12,LR,PC,PSR

SysTick_Handler                ; 1) R0-R3,R12,LR,PC,PSR Are saved on stack by the processor
    CPSID   I                  ; 2) Prevent interrupt during switch

    PUSH    {R4-R11}           ; 3) Save remaining registers, R4-R11
    LDR     R0, =RunPt         ; 4) Load pointer to RunPt (current thread) into R0
    LDR     R1, [R0]           ;    R1 = RunPt
    STR     SP, [R1]           ; 5) Save SP into TCB (RunPt->sp)

    ; C side Scheduler start
    PUSH    {R0,LR}            ; 6) Save R0 (pointer to RunPt) and LR
    BL      Scheduler          ; 7) Call Scheduler from C Side
    POP     {R0, LR}           ; 8) Restore R0 and LR
    LDR     R1, [R0]           ; 9) RunPt has been updated, load new address to R1
	; C side Scheduler end

    LDR     SP, [R1]           ; 10) Load into SP the SP of the new thread (SP = RunPt->sp)
    POP     {R4-R11}           ; 11) restore register R4-R11 from stack

    CPSIE   I                  ; 12) tasks run with interrupts enabled
    BX      LR                 ; 13) restore R0-R3,R12,LR,PC,PSR

StartOS
    LDR     R0, =RunPt         ; Pointer to RunPt, We start with first thread, which is in RunPt
    LDR     R1, [R0]           ; Load value of RunPt
    LDR     SP, [R1]           ; First thread's stack pointer
    POP     {R4-R11}           ; restore registers R4-R11
    POP     {R0-R3}            ; restore registers R0-R3
                               ;  - this is the order they are stored on stack
    POP     {R12}              ; restore register R12
    ADD     SP,#4              ; Discard LR (return address) from stack
    POP     {LR}               ; Pop start location to LR so we know where to return
    ADD     SP,#4              ; Discard PSR (Processor Status Register)

    CPSIE   I                  ; Enable interrupts at processor level
    BX      LR                 ; start first thread

    ALIGN
    END
