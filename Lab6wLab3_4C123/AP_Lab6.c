// AP_Lab6.c
// Runs on either MSP432 or TM4C123
// see GPIO.c file for hardware connections 

// Daniel Valvano and Jonathan Valvano
// November 20, 2016
// CC2650 booster or CC2650 LaunchPad, CC2650 needs to be running SimpleNP 2.2 (POWERSAVE)

#include <stdint.h>
#include <string.h>
#include "../inc/UART0.h"
#include "../inc/UART1.h"
#include "../inc/AP.h"
#include "AP_Lab6.h"
//**debug macros**APDEBUG defined in AP.h********
#ifdef APDEBUG
#define OutString(STRING) UART0_OutString(STRING)
#define OutUHex(NUM) UART0_OutUHex(NUM)
#define OutUHex2(NUM) UART0_OutUHex2(NUM)
#define OutChar(N) UART0_OutChar(N)
#else
#define OutString(STRING)
#define OutUHex(NUM)
#define OutUHex2(NUM)
#define OutChar(N)
#endif

//****links into AP.c**************
extern const uint32_t RECVSIZE;
extern uint8_t RecvBuf[];
typedef struct characteristics{
  uint16_t theHandle;          // each object has an ID
  uint16_t size;               // number of bytes in user data (1,2,4,8)
  uint8_t *pt;                 // pointer to user data, stored little endian
  void (*callBackRead)(void);  // action if SNP Characteristic Read Indication
  void (*callBackWrite)(void); // action if SNP Characteristic Write Indication
}characteristic_t;
extern const uint32_t MAXCHARACTERISTICS;
extern uint32_t CharacteristicCount;
extern characteristic_t CharacteristicList[];
typedef struct NotifyCharacteristics{
  uint16_t uuid;               // user defined 
  uint16_t theHandle;          // each object has an ID (used to notify)
  uint16_t CCCDhandle;         // generated/assigned by SNP
  uint16_t CCCDvalue;          // sent by phone to this object
  uint16_t size;               // number of bytes in user data (1,2,4,8)
  uint8_t *pt;                 // pointer to user data array, stored little endian
  void (*callBackCCCD)(void);  // action if SNP CCCD Updated Indication
}NotifyCharacteristic_t;
extern const uint32_t NOTIFYMAXCHARACTERISTICS;
extern uint32_t NotifyCharacteristicCount;
extern NotifyCharacteristic_t NotifyCharacteristicList[];
//**************Lab 6 routines*******************
// **********SetFCS**************
// helper function, add check byte to message
// assumes every byte in the message has been set except the FCS
// used the length field, assumes less than 256 bytes
// FCS = 8-bit EOR(all bytes except SOF and the FCS itself)
// Inputs: pointer to message
//         stores the FCS into message itself
// Outputs: none
void SetFCS(uint8_t *msg){

  // SetFCS - FCS is a value at the end of the message that is a simple
  // checksum (bytes XORed) of all the data in the frame.

  // SOF (1 byte) | LEN (2 bytes LE) | COMMAND (2 bytes) | PAYLOAD (LEN bytes) | FCS
  uint8_t fcs = 0;

  // skip SOF
  uint32_t idx = 1;

  // only read first byte as it's Little-Endian and we don't
  // have very long payloads
  uint8_t len = *(msg + idx++);

  // start building FCS, it's XOR of all bytes between SOF and FCS
  fcs ^= len;

  // second byte of length
  fcs ^= *(msg + idx++);

  // first byte of command
  fcs ^= *(msg + idx++);

  // second byte of command
  fcs ^= *(msg + idx++);

  // payload
  int i;
  for (i = 0; i < len; i++) {
    uint8_t byte = *(msg + idx + i);
    fcs ^= byte;
  }

  // append FCS to the end of msg
  *(msg + idx + i) = fcs;
}

//*************BuildGetStatusMsg**************
// Create a Get Status message, used in Lab 6
// Inputs pointer to empty buffer of at least 6 bytes
// Output none
// build the necessary NPI message that will Get Status
void BuildGetStatusMsg(uint8_t *msg){

  // SOF,0x00,0x00,0x55,0x06,0x53
  uint32_t idx = 0;

  *(msg + idx++) = SOF;

  // length
  *(msg + idx++) = 0x00;
  *(msg + idx++) = 0x00;

  // payload
  *(msg + idx++) = 0x55;
  *(msg + idx++) = 0x06;

  SetFCS(msg);
}
//*************Lab6_GetStatus**************
// Get status of connection, used in Lab 6
// Input:  none
// Output: status 0xAABBCCDD
// AA is GAPRole Status
// BB is Advertising Status
// CC is ATT Status
// DD is ATT method in progress
uint32_t Lab6_GetStatus(void){volatile int r; uint8_t sendMsg[8];
  OutString("\n\rGet Status");
  BuildGetStatusMsg(sendMsg);
  r = AP_SendMessageResponse(sendMsg,RecvBuf,RECVSIZE);
  return (RecvBuf[4]<<24)+(RecvBuf[5]<<16)+(RecvBuf[6]<<8)+(RecvBuf[7]);
}

//*************BuildGetVersionMsg**************
// Create a Get Version message, used in Lab 6
// Inputs pointer to empty buffer of at least 6 bytes
// Output none
// build the necessary NPI message that will Get Status
void BuildGetVersionMsg(uint8_t *msg){

  // SOF,0x00,0x00,0x35,0x03,0x36
  uint32_t idx = 0;

  *(msg + idx++) = SOF;

  // length
  *(msg + idx++) = 0x00;
  *(msg + idx++) = 0x00;

  // command
  *(msg + idx++) = 0x35;
  *(msg + idx++) = 0x03;

  SetFCS(msg);
}
//*************Lab6_GetVersion**************
// Get version of the SNP application running on the CC2650, used in Lab 6
// Input:  none
// Output: version
uint32_t Lab6_GetVersion(void){volatile int r;uint8_t sendMsg[8];
  OutString("\n\rGet Version");
  BuildGetVersionMsg(sendMsg);
  r = AP_SendMessageResponse(sendMsg,RecvBuf,RECVSIZE); 
  return (RecvBuf[5]<<8)+(RecvBuf[6]);
}

//*************BuildAddServiceMsg**************
// Create an Add service message, used in Lab 6
// Inputs uuid is 0xFFF0, 0xFFF1, ...
//        pointer to empty buffer of at least 9 bytes
// Output none
// build the necessary NPI message that will add a service
void BuildAddServiceMsg(uint16_t uuid, uint8_t *msg){
//  SOF,3,0x00,     // length = 3
//  0x35,0x81,      // SNP Add Service
//  0x01,           // Primary Service
//  0xF0,0xFF,      // UUID
//  0xB9            // FCS

  uint32_t idx = 0;

  *(msg + idx++) = SOF;

  // Little-Endian length
  *(msg + idx++) = 0x03;
  *(msg + idx++) = 0x00;

  // SNP Add Service command
  *(msg + idx++) = 0x35;
  *(msg + idx++) = 0x81;

  // parameter: Primary Service
  *(msg + idx++) = 0x01;

  // parameter: UUID
  uint8_t uuid1 = uuid & 0xff;
  *(msg + idx++) = uuid1;

  uint8_t uuid2 = uuid >> 8;
  *(msg + idx++) = uuid2;

  SetFCS(msg);
}
//*************Lab6_AddService**************
// Add a service, used in Lab 6
// Inputs uuid is 0xFFF0, 0xFFF1, ...
// Output APOK if successful,
//        APFAIL if SNP failure
int Lab6_AddService(uint16_t uuid){ int r; uint8_t sendMsg[12];
  OutString("\n\rAdd service");
  BuildAddServiceMsg(uuid,sendMsg);
  r = AP_SendMessageResponse(sendMsg,RecvBuf,RECVSIZE);  
  return r;
}
//*************AP_BuildRegisterServiceMsg**************
// Create a register service message, used in Lab 6
// Inputs pointer to empty buffer of at least 6 bytes
// Output none
// build the necessary NPI message that will register a service
void BuildRegisterServiceMsg(uint8_t *msg){
//  SOF,0x00,0x00,  // length = 0
//  0x35,0x84,      // SNP Register Service
//  0x00
  uint32_t idx = 0;

  *(msg + idx++) = SOF;

  // Little-Endian length
  *(msg + idx++) = 0x00;
  *(msg + idx++) = 0x00;

  // SNP Add Register command
  *(msg + idx++) = 0x35;
  *(msg + idx++) = 0x84;

  SetFCS(msg);
}
//*************Lab6_RegisterService**************
// Register a service, used in Lab 6
// Inputs none
// Output APOK if successful,
//        APFAIL if SNP failure
int Lab6_RegisterService(void){ int r; uint8_t sendMsg[8];
  OutString("\n\rRegister service");
  BuildRegisterServiceMsg(sendMsg);
  r = AP_SendMessageResponse(sendMsg,RecvBuf,RECVSIZE);
  return r;
}

//*************BuildAddCharValueMsg**************
// Create a Add Characteristic Value Declaration message, used in Lab 6
// Inputs uuid is 0xFFF0, 0xFFF1, ...
//        permission is GATT Permission, 0=none,1=read,2=write, 3=Read+write 
//        properties is GATT Properties, 2=read,8=write,0x0A=read+write, 0x10=notify
//        pointer to empty buffer of at least 14 bytes
// Output none
// build the necessary NPI message that will add a characteristic value
void BuildAddCharValueMsg(uint16_t uuid,  
  uint8_t permission, uint8_t properties, uint8_t *msg){
//  SOF,0x08,0x00,  // length = 8
//  0x35,0x82,      // SNP Add Characteristic Value Declaration
//  0x03,           // 0=none,1=read,2=write, 3=Read+write, GATT Permission
//  0x0A,0x00,      // 2=read,8=write,0x0A=read+write,0x10=notify, GATT Properties
//  0x00,           // RFU
//  0x00,0x02,      // Maximum length of the attribute value=512
//  0xF1,0xFF,      // UUID
//  0xBA

  uint32_t idx = 0;

  *(msg + idx++) = SOF;

  // Little-Endian length
  *(msg + idx++) = 0x08;
  *(msg + idx++) = 0x00;

  // SNP Add Characteristic Value command
  *(msg + idx++) = 0x35;
  *(msg + idx++) = 0x82;

  // permission
  *(msg + idx++) = permission;

  // properties
  *(msg + idx++) = properties;
  *(msg + idx++) = 0x00;

  // set RFU to 0
  *(msg + idx++) = 0x00;

  // set the maximum length of the attribute value=512
  *(msg + idx++) = 0x00;
  *(msg + idx++) = 0x02;

  // parameter: UUID
  uint8_t uuid1 = uuid & 0xff;
  *(msg + idx++) = uuid1;

  uint8_t uuid2 = uuid >> 8;
  *(msg + idx++) = uuid2;

  SetFCS(msg);
// for a hint see NPI_AddCharValue in AP.c
// for a hint see first half of AP_AddCharacteristic and first half of AP_AddNotifyCharacteristic
}

//*************BuildAddCharDescriptorMsg**************
// Create a Add Characteristic Descriptor Declaration message, used in Lab 6
// Inputs name is a null-terminated string, maximum length of name is 20 bytes
//        pointer to empty buffer of at least 32 bytes
// Output none
// build the necessary NPI message that will add a Descriptor Declaration
void BuildAddCharDescriptorMsg(char name[], uint8_t *msg){
//  SOF,0x17,0x00,  // length determined at run time 6+string length
//  0x35,0x83,      // SNP Add Characteristic Descriptor Declaration
//  0x80,           // User Description String
//  0x01,           // GATT Read Permissions
//  0x11,0x00,      // Maximum Possible length of the user description string
//  0x11,0x00,      // Initial length of the user description string
//  'C','h','a','r','a','c','t','e','r','i','s','t','i','c',' ','0',0, // Initial user description string
//  0x0C,0,0,0

  uint32_t idx = 0;

  // length of descriptor
  uint32_t len = strlen(name);

  // length includes the null terminator
  len++;

  // Start Of Frame
  *(msg + idx++) = SOF;

  // length of payload = descriptor length + 6 (null terminator)
  *(msg + idx++) = (uint8_t)len + 6;
  *(msg + idx++) = 0x00;

  // SNP Add Characteristic Descriptior Declaration command
  *(msg + idx++) = 0x35;
  *(msg + idx++) = 0x83;

  // User Description String
  *(msg + idx++) = 0x80;
  
  // Read Permission
  *(msg + idx++) = 0x01;

  // maxlength of descriptor
  *(msg + idx++) = (uint8_t)len;
  *(msg + idx++) = 0x00;

  // initial length of descriptor
  *(msg + idx++) = (uint8_t)len;
  *(msg + idx++) = 0x00;

  // set descriptor to name
  int i;
  for (i = 0; i < len; i++) {
    *(msg + idx + i) = name[i];
  }

  // append null terminator
  *(msg + idx + i + 1) = 0;

  SetFCS(msg);
}

//*************Lab6_AddCharacteristic**************
// Add a read, write, or read/write characteristic, used in Lab 6
//        for notify properties, call AP_AddNotifyCharacteristic 
// Inputs uuid is 0xFFF0, 0xFFF1, ...
//        thesize is the number of bytes in the user data 1,2,4, or 8 
//        pt is a pointer to the user data, stored little endian
//        permission is GATT Permission, 0=none,1=read,2=write, 3=Read+write 
//        properties is GATT Properties, 2=read,8=write,0x0A=read+write
//        name is a null-terminated string, maximum length of name is 20 bytes
//        (*ReadFunc) called before it responses with data from internal structure
//        (*WriteFunc) called after it accepts data into internal structure
// Output APOK if successful,
//        APFAIL if name is empty, more than 8 characteristics, or if SNP failure
int Lab6_AddCharacteristic(uint16_t uuid, uint16_t thesize, void *pt, uint8_t permission,
  uint8_t properties, char name[], void(*ReadFunc)(void), void(*WriteFunc)(void)){
  int r; uint16_t handle; 
  uint8_t sendMsg[32];  
  if(thesize>8) return APFAIL;
  if(name[0]==0) return APFAIL;       // empty name
  if(CharacteristicCount>=MAXCHARACTERISTICS) return APFAIL; // error
  BuildAddCharValueMsg(uuid,permission,properties,sendMsg);
  OutString("\n\rAdd CharValue");
  r=AP_SendMessageResponse(sendMsg,RecvBuf,RECVSIZE);
  if(r == APFAIL) return APFAIL;
  handle = (RecvBuf[7]<<8)+RecvBuf[6]; // handle for this characteristic
  OutString("\n\rAdd CharDescriptor");
  BuildAddCharDescriptorMsg(name,sendMsg);
  r=AP_SendMessageResponse(sendMsg,RecvBuf,RECVSIZE);
  if(r == APFAIL) return APFAIL;
  CharacteristicList[CharacteristicCount].theHandle = handle;
  CharacteristicList[CharacteristicCount].size = thesize;
  CharacteristicList[CharacteristicCount].pt = (uint8_t *) pt;
  CharacteristicList[CharacteristicCount].callBackRead = ReadFunc;
  CharacteristicList[CharacteristicCount].callBackWrite = WriteFunc;
  CharacteristicCount++;
  return APOK; // OK
} 
  

//*************BuildAddNotifyCharDescriptorMsg**************
// Create a Add Notify Characteristic Descriptor Declaration message, used in Lab 6
// Inputs name is a null-terminated string, maximum length of name is 20 bytes
//        pointer to empty buffer of at least bytes
// Output none
// build the necessary NPI message that will add a Descriptor Declaration
void BuildAddNotifyCharDescriptorMsg(char name[], uint8_t *msg){
//  SOF,0x17,0x00,  // length determined at run time 7+string length
//  0x35,0x83,      // SNP Add Characteristic Descriptor Declaration
//  0x84,           // User Description String + CCCD
//  0x03,           // CCCD Permissions
//  0x01,           // GATT Read Permissions
//  0x11,0x00,      // Maximum Possible length of the user description string
//  0x11,0x00,      // Initial length of the user description string
//  'C','h','a','r','a','c','t','e','r','i','s','t','i','c',' ','0',0, // Initial user description string
//  0x0C            // FCS
  uint32_t idx = 0;

  // length of descriptor
  uint32_t len = strlen(name);

  // length includes the null terminator
  len++;

  // Start Of Frame
  *(msg + idx++) = SOF;

  // length of payload = descriptor length + 7
  *(msg + idx++) = (uint8_t)len + 7;
  *(msg + idx++) = 0x00;

  // SNP Add Characteristic Descriptior Declaration command
  *(msg + idx++) = 0x35;
  *(msg + idx++) = 0x83;

  // User Description String + CCCD
  *(msg + idx++) = 0x84;
  
  // CCCD parameters read+write
  *(msg + idx++) = 0x03;
  
  // Read Permission
  *(msg + idx++) = 0x01;

  // maxlength of descriptor
  *(msg + idx++) = (uint8_t)len;
  *(msg + idx++) = 0x00;

  // initial length of descriptor
  *(msg + idx++) = (uint8_t)len;
  *(msg + idx++) = 0x00;

  // set descriptor to name
  int i;
  for (i = 0; i < len; i++) {
    *(msg + idx + i) = name[i];
  }

  // append null terminator
  *(msg + idx + i + 1) = 0;

  SetFCS(msg);
}
  
//*************Lab6_AddNotifyCharacteristic**************
// Add a notify characteristic, used in Lab 6
//        for read, write, or read/write characteristic, call AP_AddCharacteristic 
// Inputs uuid is 0xFFF0, 0xFFF1, ...
//        thesize is the number of bytes in the user data 1,2,4, or 8 
//        pt is a pointer to the user data, stored little endian
//        name is a null-terminated string, maximum length of name is 20 bytes
//        (*CCCDfunc) called after it accepts , changing CCCDvalue
// Output APOK if successful,
//        APFAIL if name is empty, more than 4 notify characteristics, or if SNP failure
int Lab6_AddNotifyCharacteristic(uint16_t uuid, uint16_t thesize, void *pt,   
  char name[], void(*CCCDfunc)(void)){
  int r; uint16_t handle; 
  uint8_t sendMsg[36];  
  if(thesize>8) return APFAIL;
  if(NotifyCharacteristicCount>=NOTIFYMAXCHARACTERISTICS) return APFAIL; // error
  BuildAddCharValueMsg(uuid,0,0x10,sendMsg);
  OutString("\n\rAdd Notify CharValue");
  r=AP_SendMessageResponse(sendMsg,RecvBuf,RECVSIZE);
  if(r == APFAIL) return APFAIL;
  handle = (RecvBuf[7]<<8)+RecvBuf[6]; // handle for this characteristic
  OutString("\n\rAdd CharDescriptor");
  BuildAddNotifyCharDescriptorMsg(name,sendMsg);
  r=AP_SendMessageResponse(sendMsg,RecvBuf,RECVSIZE);
  if(r == APFAIL) return APFAIL;
  NotifyCharacteristicList[NotifyCharacteristicCount].uuid = uuid;
  NotifyCharacteristicList[NotifyCharacteristicCount].theHandle = handle;
  NotifyCharacteristicList[NotifyCharacteristicCount].CCCDhandle = (RecvBuf[8]<<8)+RecvBuf[7]; // handle for this CCCD
  NotifyCharacteristicList[NotifyCharacteristicCount].CCCDvalue = 0; // notify initially off
  NotifyCharacteristicList[NotifyCharacteristicCount].size = thesize;
  NotifyCharacteristicList[NotifyCharacteristicCount].pt = (uint8_t *) pt;
  NotifyCharacteristicList[NotifyCharacteristicCount].callBackCCCD = CCCDfunc;
  NotifyCharacteristicCount++;
  return APOK; // OK
}

//*************BuildSetDeviceNameMsg**************
// Create a Set GATT Parameter message, used in Lab 6
// Inputs name is a null-terminated string, maximum length of name is 24 bytes
//        pointer to empty buffer of at least 36 bytes
// Output none
// build the necessary NPI message to set Device name
void BuildSetDeviceNameMsg(char name[], uint8_t *msg){
//  SOF,22,0x00,    // length = 22
//  0x35,0x8C,      // SNP Set GATT Parameter (0x8C)
//  0x01,           // Generic Access Service
//  0x00,0x00,      // Device Name
//  'S','h','a','p','e',' ','t','h','e',' ','W','o','r','l','d',' ','0','0','1',
//  0x77

  uint32_t idx = 0;

  // length of descriptor
  uint32_t len = strlen(name);

  // Start Of Frame
  *(msg + idx++) = SOF;

  // length of payload = name length + 3
  *(msg + idx++) = (uint8_t)len + 3;
  *(msg + idx++) = 0x00;

  // SNP Set GATT Parameter command
  *(msg + idx++) = 0x35;
  *(msg + idx++) = 0x8c;

  // Generic Access Service
  *(msg + idx++) = 0x01;
  
  // we are setting device name
  *(msg + idx++) = 0x00;
  *(msg + idx++) = 0x00;
  
  // append device name
  int i;
  for (i = 0; i < len; i++) {
    *(msg + idx + i) = name[i];
  }

  // append null terminator
  *(msg + idx + i + 1) = 0;

  SetFCS(msg);
}
//*************BuildSetAdvertisementData1Msg**************
// Create a Set Advertisement Data message, used in Lab 6
// Inputs pointer to empty buffer of at least 16 bytes
// Output none
// build the necessary NPI message for Non-connectable Advertisement Data
void BuildSetAdvertisementData1Msg(uint8_t *msg){
//  SOF,11,0x00,    // length = 11
//  0x55,0x43,      // SNP Set Advertisement Data
//  0x01,           // Not connected Advertisement Data
//  0x02,0x01,0x06, // GAP_ADTYPE_FLAGS,DISCOVERABLE | no BREDR
//  0x06,0xFF,      // length, manufacturer specific
//  0x0D ,0x00,     // Texas Instruments Company ID
//  0x03,           // TI_ST_DEVICE_ID
//  0x00,           // TI_ST_KEY_DATA_ID
//  0x00,           // Key state
//  0xEE            // FCS

  uint32_t idx = 0;

  // Start Of Frame
  *(msg + idx++) = SOF;

  // length
  *(msg + idx++) = 0x0b;
  *(msg + idx++) = 0x00;
  
  // SNP Set Advertisemenet Data command
  *(msg + idx++) = 0x55;
  *(msg + idx++) = 0x43;

  // Non-connectable Advertisement Data
  *(msg + idx++) = 0x01;

  // GAP_ADTYPE_FLAGS,DISCOVERABLE | no BREDR
  // length of this field
  *(msg + idx++) = 0x02;
  
  // advertising type flags
  *(msg + idx++) = 0x01;

  // general discovery, no BREDR
  *(msg + idx++) = 0x06;

  // length, manufacturer specific
  *(msg + idx++) = 0x06;
  *(msg + idx++) = 0xff;

  // Texas Instruments Company ID
  *(msg + idx++) = 0x0d;
  *(msg + idx++) = 0x00;

  // TI_ST_DEVICE_ID = 3
  *(msg + idx++) = 0x03;

  // TI_ST_KEY_DATA_ID
  *(msg + idx++) = 0x00;

  // Key state=0
  *(msg + idx++) = 0x00;

  SetFCS(msg);
}

//*************BuildSetAdvertisementDataMsg**************
// Create a Set Advertisement Data message, used in Lab 6
// Inputs name is a null-terminated string, maximum length of name is 24 bytes
//        pointer to empty buffer of at least 36 bytes
// Output none
// build the necessary NPI message for Scan Response Data
void BuildSetAdvertisementDataMsg(char name[], uint8_t *msg){
//  SOF,31,0x00,    // length = 31
//  0x55,0x43,      // SNP Set Advertisement Data
//  0x00,           // Scan Response Data
//  20,0x09,        // length, type=LOCAL_NAME_COMPLETE
//  'S','h','a','p','e',' ','t','h','e',' ','W','o','r','l','d',' ','S','A','P',
// - connection interval range
//  0x05,           // length of this data
//  0x12,           // GAP_ADTYPE_SLAVE_CONN_INTERVAL_RANGE
//  0x50,0x00,      // DEFAULT_DESIRED_MIN_CONN_INTERVAL
//  0x20,0x03,      // DEFAULT_DESIRED_MAX_CONN_INTERVAL
// - Tx power level
//  0x02,           // length of this data
//  0x0A,           // GAP_ADTYPE_POWER_LEVEL
//  0x00,           // 0dBm
//  0x77            // FCS
  uint32_t idx = 0;

  // Start Of Frame
  *(msg + idx++) = SOF;

  // response payload length
  const int len = strlen(name);

  // length = name length + 12
  *(msg + idx++) = len + 12;
  *(msg + idx++) = 0x00;
  
  // SNP Set Advertisemenet Data command
  *(msg + idx++) = 0x55;
  *(msg + idx++) = 0x43;

  // Scan Response Data
  *(msg + idx++) = 0x00;

  // length = name length + null terminator
  *(msg + idx++) = len + 1;

  // response type = LOCAL_NAME_COMPLETE
  *(msg + idx++) = 0x09;

  int i;
  for (i = 0; i < len; i++) {
    *(msg + idx + i) = name[i];
  }

  // update idx with out message length
  idx = idx + i;

  // XXX: couldn't find rest of these values in the datasheet??

  // connection interval range start
  // length of this data
  *(msg + idx++) = 0x05;
  // GAP_ADTYPE_SLAVE_CONN_INTERVAL_RANGE
  *(msg + idx++) = 0x12;

  // DEFAULT_DESIRED_MIN_CONN_INTERVAL
  *(msg + idx++) = 0x50;
  *(msg + idx++) = 0x00;

  // DEFAULT_DESIRED_MAX_CONN_INTERVAL
  *(msg + idx++) = 0x20;
  *(msg + idx++) = 0x03;
  // connection interval range end

  // Tx power level start
  // length of this data
  *(msg + idx++) = 0x02;
  // GAP_ADTYPE_POWER_LEVEL
  *(msg + idx++) = 0x0a;
  // 0dBm
  *(msg + idx++) = 0x00;
  // Tx power level end

  SetFCS(msg);
}
//*************BuildStartAdvertisementMsg**************
// Create a Start Advertisement Data message, used in Lab 6
// Inputs advertising interval
//        pointer to empty buffer of at least 20 bytes
// Output none
// build the necessary NPI message to start advertisement
void BuildStartAdvertisementMsg(uint16_t interval, uint8_t *msg){
//  SOF,14,0x00,    // length = 14
//  0x55,0x42,      // SNP Start Advertisement
//  0x00,           // Connectable Undirected Advertisements
//  0x00,0x00,      // Advertise infinitely.
//  0x64,0x00,      // Advertising Interval (100 * 0.625 ms=62.5ms)
//  0x00,           // Filter Policy RFU
//  0x00,           // Initiator Address Type RFU
//  0x00,0x01,0x00,0x00,0x00,0xC5, // RFU
//  0x02,           // Advertising will restart with connectable advertising when a connection is terminated
//  0xBB

  uint32_t idx = 0;

  // Start Of Frame
  *(msg + idx++) = SOF;

  // length
  *(msg + idx++) = 0x0e;
  *(msg + idx++) = 0x00;
  
  // SNP Start Advertisemenet command
  *(msg + idx++) = 0x55;
  *(msg + idx++) = 0x42;

  // Connectable Undirected Advertisements
  *(msg + idx++) = 0x00;

  // Advertise infinitely
  *(msg + idx++) = 0x00;
  *(msg + idx++) = 0x00;

  // Advertising Interval
  uint8_t i1 = interval & 0xff;
  uint8_t i2 = interval >> 8;
  
  *(msg + idx++) = i1;
  *(msg + idx++) = i2;

  // Filter Policy RFU
  *(msg + idx++) = 0x00;

  // Initiator Address Type RFU
  *(msg + idx++) = 0x00;

  // RFU
  *(msg + idx++) = 0x00;
  *(msg + idx++) = 0x01;
  *(msg + idx++) = 0x00;
  *(msg + idx++) = 0x00;
  *(msg + idx++) = 0x00;
  *(msg + idx++) = 0xc5;

  // Advertising will restart with connectable advertising when a connection is terminated
  *(msg + idx++) = 0x02;

  SetFCS(msg);
}

//*************Lab6_StartAdvertisement**************
// Start advertisement, used in Lab 6
// Input:  none
// Output: APOK if successful,
//         APFAIL if notification not configured, or if SNP failure
int Lab6_StartAdvertisement(void){volatile int r; uint8_t sendMsg[40];
  OutString("\n\rSet Device name");
  BuildSetDeviceNameMsg("Shape the World",sendMsg);
  r =AP_SendMessageResponse(sendMsg,RecvBuf,RECVSIZE);
  OutString("\n\rSetAdvertisement1");
  BuildSetAdvertisementData1Msg(sendMsg);
  r =AP_SendMessageResponse(sendMsg,RecvBuf,RECVSIZE);
  OutString("\n\rSetAdvertisement Data");
  BuildSetAdvertisementDataMsg("Shape the World",sendMsg);
  r =AP_SendMessageResponse(sendMsg,RecvBuf,RECVSIZE);
  OutString("\n\rStartAdvertisement");
  BuildStartAdvertisementMsg(100,sendMsg);
  r =AP_SendMessageResponse(sendMsg,RecvBuf,RECVSIZE);
  return r;
}

