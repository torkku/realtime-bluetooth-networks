// os.c
// Runs on LM4F120/TM4C123/MSP432
// Lab 3 starter file.
// Daniel Valvano
// March 24, 2016

#include <stdint.h>
#include "os.h"
#include "CortexM.h"
#include "BSP.h"

// function definitions in osasm.s
void StartOS(void);

#define NUMTHREADS  6        // maximum number of threads
#define NUMPERIODIC 2        // maximum number of periodic threads
#define STACKSIZE   100      // number of 32-bit words in stack per thread

// thread control block
struct tcb{
  int32_t *sp;       // pointer to stack (valid for threads not running
  struct tcb *next;  // linked-list pointer

  // nonzero if blocked on this semaphore
  // nonzero if this thread is sleeping
  // when blocked holds pointer to the semaphore
  int32_t *blocked;

  // if nonzero then the thread is sleeping
  uint32_t sleep;
};

typedef struct tcb tcbType;
tcbType tcbs[NUMTHREADS];
tcbType *RunPt;
int32_t Stacks[NUMTHREADS][STACKSIZE];

void static wakeupthreads(void){
  // decrement sleep counter for all threads
  // that are in sleep
  for (int i = 0; i < NUMTHREADS; i++) {
    if (tcbs[i].sleep) {
      tcbs[i].sleep--;
    }
  }
}

// ******** OS_Init ************
// Initialize operating system, disable interrupts
// Initialize OS controlled I/O: periodic interrupt, bus clock as fast as possible
// Initialize OS global variables
// Inputs:  none
// Outputs: none
void OS_Init(void){
  DisableInterrupts();
  BSP_Clock_InitFastest();// set processor clock to fastest speed

  // perform any initializations needed

  // set up periodic task to wake up sleeping threads
  BSP_PeriodicTask_Init(&wakeupthreads, 1000, 2);
}

void SetInitialStack(int i){
  tcbs[i].sp = &Stacks[i][STACKSIZE-16]; // initial thread stack pointer
  Stacks[i][STACKSIZE-1] = 0x01000000;   // thumb bit

  // Stacks[i][STACKSIZE-2] will be set later when we know the PC

  // these values does not matter, they are just random initial values
  // I chose something readable, for example 0x41414141 is "AAAA"
  Stacks[i][STACKSIZE-3] = 0x14141414;   // R14
  Stacks[i][STACKSIZE-4] = 0x12121212;   // R12
  Stacks[i][STACKSIZE-5] = 0x03030303;   // R3
  Stacks[i][STACKSIZE-6] = 0x02020202;   // R2
  Stacks[i][STACKSIZE-7] = 0x01010101;   // R1
  Stacks[i][STACKSIZE-8] = 0x00000000;   // R0
  Stacks[i][STACKSIZE-9] = 0x11111111;   // R11
  Stacks[i][STACKSIZE-10] = 0x10101010;  // R10
  Stacks[i][STACKSIZE-11] = 0x09090909;  // R9
  Stacks[i][STACKSIZE-12] = 0x08080808;  // R8
  Stacks[i][STACKSIZE-13] = 0x07070707;  // R7
  Stacks[i][STACKSIZE-14] = 0x06060606;  // R6
  Stacks[i][STACKSIZE-15] = 0x05050505;  // R5
  Stacks[i][STACKSIZE-16] = 0x04040404;  // R4
}

//******** OS_AddThreads ***************
// Add six main threads to the scheduler
// Inputs: function pointers to six void/void main threads
// Outputs: 1 if successful, 0 if this thread can not be added
// This function will only be called once, after OS_Init and before OS_Launch
int OS_AddThreads(void(*thread0)(void),
                  void(*thread1)(void),
                  void(*thread2)(void),
                  void(*thread3)(void),
                  void(*thread4)(void),
                  void(*thread5)(void)){

  // start critical section
  int32_t status;
  status = StartCritical();

  // initialize TCB circular list
  tcbs[0].next = &tcbs[1]; // 0 -> 1
  tcbs[1].next = &tcbs[2]; // 1 -> 2
  tcbs[2].next = &tcbs[3]; // 2 -> 3
  tcbs[3].next = &tcbs[4]; // 3 -> 4
  tcbs[4].next = &tcbs[5]; // 4 -> 5
  tcbs[5].next = &tcbs[0]; // 5 -> 0

  // initialize all tcbs as not blocked
  tcbs[0].blocked = 0;
  tcbs[1].blocked = 0;
  tcbs[2].blocked = 0;
  tcbs[3].blocked = 0;
  tcbs[4].blocked = 0;
  tcbs[5].blocked = 0;

  // initialize all tcbs as not sleeping
  tcbs[0].sleep = 0;
  tcbs[1].sleep = 0;
  tcbs[2].sleep = 0;
  tcbs[3].sleep = 0;
  tcbs[4].sleep = 0;
  tcbs[5].sleep = 0;

  // initialize stacks
  SetInitialStack(0);
  SetInitialStack(1);
  SetInitialStack(2);
  SetInitialStack(3);
  SetInitialStack(4);
  SetInitialStack(5);

  // store initial program counters to stacks
  // ie. make them point to the tasks
  Stacks[0][STACKSIZE-2] = (int32_t)(thread0);
  Stacks[1][STACKSIZE-2] = (int32_t)(thread1);
  Stacks[2][STACKSIZE-2] = (int32_t)(thread2);
  Stacks[3][STACKSIZE-2] = (int32_t)(thread3);
  Stacks[4][STACKSIZE-2] = (int32_t)(thread4);
  Stacks[5][STACKSIZE-2] = (int32_t)(thread5);

  // initialize RunPt to first task
  RunPt = &tcbs[0];

  // end critical section
  EndCritical(status);

  return 1;               // successful
}

//******** OS_AddPeriodicEventThread ***************
// Add one background periodic event thread
// Typically this function receives the highest priority
// Inputs: pointer to a void/void event thread function
//         period given in units of OS_Launch (Lab 3 this will be msec)
// Outputs: 1 if successful, 0 if this thread cannot be added
// It is assumed that the event threads will run to completion and return
// It is assumed the time to run these event threads is short compared to 1 msec
// These threads cannot spin, block, loop, sleep, or kill
// These threads can call OS_Signal
// In Lab 3 this will be called exactly twice
int32_t numPeriodic = 0;
int OS_AddPeriodicEventThread(void(*thread)(void), uint32_t period){

  // check that we don't have max number of periodic
  // event threads already
  if (numPeriodic >= NUMPERIODIC) {
    return -1; // failure
  }

  // call Board Support Package to start this periodic
  // event thread
  if (numPeriodic == 0) {
    BSP_PeriodicTask_InitB(thread, 1000/period, 1);
  } else {
    BSP_PeriodicTask_InitC(thread, 1000/period, 1);
  }

  // we now have one more periodic event threads
  numPeriodic++;

  return 1;
}


//******** OS_Launch ***************
// Start the scheduler, enable interrupts
// Inputs: number of clock cycles for each time slice
// Outputs: none (does not return)
// Errors: theTimeSlice must be less than 16,777,216
void OS_Launch(uint32_t theTimeSlice){
  STCTRL = 0;                  // disable SysTick during setup
  STCURRENT = 0;               // any write to current clears it
  SYSPRI3 =(SYSPRI3&0x00FFFFFF)|0xE0000000; // priority 7
  STRELOAD = theTimeSlice - 1; // reload value
  STCTRL = 0x00000007;         // enable, core clock and interrupt arm
  StartOS();                   // start on the first task
}
// runs every ms
void Scheduler(void){ // every time slice

  // Round robin, skips blocked threads

  // let's start searching from the first one
  RunPt = RunPt->next;

  // look for a thread that is not blocked
  // and that is also not sleeping
  while (RunPt->blocked || RunPt->sleep) {
    RunPt = RunPt->next;
  }

}

//******** OS_Suspend ***************
// Called by main thread to cooperatively suspend operation
// Inputs: none
// Outputs: none
// Will be run again depending on sleep/block status
void OS_Suspend(void){
  STCURRENT = 0;        // any write to current clears it
  INTCTRL = 0x04000000; // trigger SysTick
// next thread gets a full time slice
}

// ******** OS_Sleep ************
// place this thread into a dormant state
// input:  number of msec to sleep
// output: none
// OS_Sleep(0) implements cooperative multitasking
void OS_Sleep(uint32_t sleepTime){

  // set sleep parameter in TCB
  RunPt->sleep = sleepTime;

  // suspend, stops running
  OS_Suspend();
}

// ******** OS_InitSemaphore ************
// Initialize counting semaphore
// Inputs:  pointer to a semaphore
//          initial value of semaphore
// Outputs: none
void OS_InitSemaphore(int32_t *semaPt, int32_t value){
  *semaPt = value;
}

// ******** OS_Wait ************
// Decrement semaphore and block if less than zero
// Lab2 spinlock (does not suspend while spinning)
// Lab3 block if less than zero
// Inputs:  pointer to a counting semaphore
// Outputs: none
void OS_Wait(int32_t *semaPt){

  // start critical section
  DisableInterrupts();

  // decrease the semaphore
  *semaPt = *semaPt - 1;

  // is it blocked?
  if (*semaPt < 0) {

    // this is the reason why it's blocked
    RunPt->blocked = semaPt;

    // let's someone else have a chance to do their stuff
    EnableInterrupts();

    // triggers the systick so we are done,
    // ie back to the scheduler
    OS_Suspend();
  }

  // end critical section
  EnableInterrupts();
}

// ******** OS_Signal ************
// Increment semaphore
// Lab2 spinlock
// Lab3 wakeup blocked thread if appropriate
// Inputs:  pointer to a counting semaphore
// Outputs: none
void OS_Signal(int32_t *semaPt){

  // using this to interate through TCB's
  tcbType *pt;

  // start critical section
  DisableInterrupts();

  // increase the semaphore as we are not waiting anymore
  *semaPt = *semaPt + 1;

  // something should be blocked
  if ((*semaPt) <= 0) {
    // start from the first thread after current
    pt = RunPt->next;

    // look for blocked thread waiting on this semaphose
    // if blocked, the field holds address of this semaphore
    while (pt->blocked != semaPt) {
      pt = pt->next;
    }

    // we found first one, let's wake it up
    pt->blocked = 0;
  }

  EnableInterrupts();
}

#define FSIZE 10    // can be any size
uint32_t PutI;      // index of where to put next
uint32_t GetI;      // index of where to get next
uint32_t Fifo[FSIZE];
int32_t CurrentSize;// 0 means FIFO empty, FSIZE means full
uint32_t LostData;  // number of lost pieces of data

// ******** OS_FIFO_Init ************
// Initialize FIFO.  
// One event thread producer, one main thread consumer
// Inputs:  none
// Outputs: none
#define FIFOSIZE 10
uint32_t PutIdx;         // index where to put next
uint32_t GetIdx;         // index where to get next
uint32_t Fifo[FIFOSIZE]; // array for the fifo
int32_t CurrentSize;     // 0 = empty, FIFOSIZE = full
uint32_t LostData;       // number of lost pieces of data

void OS_FIFO_Init(void){

  // initialize indexs for empty FIFO
  PutIdx = GetIdx = 0;

  // semaphore to guard that or fifo is not empty
  // when trying to get or full when trying to put
  OS_InitSemaphore(&CurrentSize, 0);

  // we haven't lost any data yet
  LostData = 0;

}

// ******** OS_FIFO_Put ************
// Put an entry in the FIFO.  
// Exactly one event thread puts,
// do not block or spin if full
// Inputs:  data to be stored
// Outputs: 0 if successful, -1 if the FIFO is full
int OS_FIFO_Put(uint32_t data){

  // is fifo full?
  if (CurrentSize == FIFOSIZE) {
    LostData++;
    return -1; // failed
  }

  // put the data in the fifo
  Fifo[PutIdx] = data;

  // grow the index (wrap at FIFOSIZE)
  PutIdx = (PutIdx + 1) % FIFOSIZE;

  // signal semaphore that there is data
  OS_Signal(&CurrentSize);

  return 0;   // success

}

// ******** OS_FIFO_Get ************
// Get an entry from the FIFO.   
// Exactly one main thread get,
// do block if empty
// Inputs:  none
// Outputs: data retrieved
uint32_t OS_FIFO_Get(void){

  uint32_t data;

  // wait for data
  OS_Wait(&CurrentSize);

  data = Fifo[GetIdx];

  // update the index for next get
  GetIdx = (GetIdx + 1) % FIFOSIZE;

  return data;
}



